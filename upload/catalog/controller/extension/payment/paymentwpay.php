<?php
class ControllerExtensionPaymentPaymentWpay extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
    
    $order_products = $this->model_checkout_order->getOrderProducts($this->session->data['order_id']);
    
		if ($this->config->get('payment_paymentwpay_test') == 100) {
			$data['action'] = 'https://secure-test.worldpay.com/wcc/purchase';
		} else {
			$data['action'] = 'https://secure.worldpay.com/wcc/purchase';
		}

		$data['merchant'] = $this->config->get('payment_paymentwpay_merchant');
		$data['order_id'] = $order_info['order_id'];
		$data['amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
		$data['amount'] = sprintf('%0.2f', $data['amount']);
    
    $data['currency'] = $order_info['currency_code'];
		$data['description'] = $this->config->get('config_name') . ' - #' . $order_info['order_id'];
		$data['name'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];
    
    $data["auth_valid_to"] = (time()*1000)+(60*10*1000); // auth is valid for 10 minutes
    
    $md5_secret = $this->config->get('payment_paymentwpay_md5_secret');
    
    $data['description'] = "";
    
    $desc = [];
    $cartid = [];
    
    foreach ($order_products as $key => $order_product) 
    { 
      $desc[] = $order_product['name'];
      $cartid[] = $order_product['model'];
    }
    
    $data["desc"] = "$desc[0]";
    $data["cartId"] = implode(", ", $cartid);
    
    $rawSignature = $md5_secret.":".$data['amount'].":".$order_info['currency_code'].":".$data["cartId"].":".$data['merchant'];
    
    $data["signature"] = md5($rawSignature);
    $data['signature_fields'] = $this->config->get('payment_paymentwpay_signature');
    
    $data['callback'] = $this->config->get('payment_paymentwpay_relay_response_url');
    
		if (!$order_info['payment_address_2']) {
			$data['address'] = $order_info['payment_address_1'] . ', ' . $order_info['payment_city'] . ', ' . $order_info['payment_zone'];
		} else {
			$data['address'] = $order_info['payment_address_1'] . ', ' . $order_info['payment_address_2'] . ', ' . $order_info['payment_city'] . ', ' . $order_info['payment_zone'];
		}
    
		$data['postcode'] = $order_info['payment_postcode'];
		$data['country'] = $order_info['payment_iso_code_2'];
		$data['telephone'] = $order_info['telephone'];
		$data['email'] = $order_info['email'];
		$data['test'] = $this->config->get('payment_paymentwpay_test');
    
		return $this->load->view('extension/payment/paymentwpay', $data);
	}

	public function callback() {
		$this->load->language('extension/payment/paymentwpay');

		$data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

		if (!$this->request->server['HTTPS']) {
			$data['base'] = $this->config->get('config_url');
		} else {
			$data['base'] = $this->config->get('config_ssl');
		}

		$data['language'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

		$data['text_response'] = $this->language->get('text_response');
		$data['text_success'] = $this->language->get('text_success');
		$data['text_success_wait'] = sprintf($this->language->get('text_success_wait'), $this->url->link('checkout/success'));
		$data['text_failure'] = $this->language->get('text_failure');
		$data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->url->link('checkout/checkout', '', true));

		if (isset($this->request->post['transStatus']) && $this->request->post['transStatus'] == 'Y') {
			$message = '';

			if (isset($this->request->post['transId'])) {
				$message .= 'transId: ' . $this->request->post['transId'] . "\n";
			}

			if (isset($this->request->post['transStatus'])) {
				$message .= 'transStatus: ' . $this->request->post['transStatus'] . "\n";
			}

			if (isset($this->request->post['countryMatch'])) {
				$message .= 'countryMatch: ' . $this->request->post['countryMatch'] . "\n";
			}

			if (isset($this->request->post['AVS'])) {
				$message .= 'AVS: ' . $this->request->post['AVS'] . "\n";
			}

			if (isset($this->request->post['rawAuthCode'])) {
				$message .= 'rawAuthCode: ' . $this->request->post['rawAuthCode'] . "\n";
			}

			if (isset($this->request->post['authMode'])) {
				$message .= 'authMode: ' . $this->request->post['authMode'] . "\n";
			}

			if (isset($this->request->post['rawAuthMessage'])) {
				$message .= 'rawAuthMessage: ' . $this->request->post['rawAuthMessage'] . "\n";
			}

			if (isset($this->request->post['wafMerchMessage'])) {
				$message .= 'wafMerchMessage: ' . $this->request->post['wafMerchMessage'] . "\n";
			}

			$this->load->model('checkout/order');

			// If returned successful but callbackPW doesn't match, set order to pendind and record reason
			if (isset($this->request->post['callbackPW']) && ($this->request->post['callbackPW'] == $this->config->get('payment_paymentwpay_password'))) {
				$this->model_checkout_order->addOrderHistory($this->request->post['cartId'], $this->config->get('payment_paymentwpay_order_status_id'), $message, false);
			} else {
				$this->model_checkout_order->addOrderHistory($this->request->post['cartId'], $this->config->get('config_order_status_id'), $this->language->get('text_pw_mismatch'));
			}

			$data['continue'] = $this->url->link('checkout/success');

			$this->response->setOutput($this->load->view('extension/payment/paymentwpay_success', $data));
			
		} else {
			$data['continue'] = $this->url->link('checkout/cart');

			
			$this->response->setOutput($this->load->view('extension/payment/paymentwpay_failure', $data));
		}
	}
}